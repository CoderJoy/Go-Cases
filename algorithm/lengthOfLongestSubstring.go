package main

/**
3. 无重复字符的最长子串
给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
*/

func lengthOfLongestSubstring(s string) int {
	m := make(map[byte]int)
	l, ans := 0, 0
	for r := 0; r < len(s); r++ {
		char := s[r]
		if val, ok := m[char]; ok { // 存在重复，跳转到重复索引下一个位置
			l = max(val+1, l)
		}
		ans = max(ans, r-l+1)
		m[char] = r
	}
	return ans
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
