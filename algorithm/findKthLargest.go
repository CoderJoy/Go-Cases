package main

/**
215. 数组中的第K个最大元素
给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。

请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。

你必须设计并实现时间复杂度为 O(n) 的算法解决此问题。

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/kth-largest-element-in-an-array
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
*/

func findKthLargest(nums []int, k int) int {
	size := len(nums)
	buildMaxHeap(nums, size)
	for i := len(nums) - 1; i >= len(nums)-k+1; i-- {
		// 每次交换并固定堆顶最大值到数组末端,k-1 次后 nums[0] 就是最大值
		nums[0], nums[i] = nums[i], nums[0]
		// 相当于删除堆顶元素
		size--
		maxHeapify(nums, 0, size)
	}
	return nums[0]
}

func buildMaxHeap(nums []int, size int) {
	// 从最后一个父结点位置调整子树
	for i := size / 2; i >= 0; i-- {
		maxHeapify(nums, i, size)
	}
}

/**
调整当前结点和子结点的堆顺序
*/
func maxHeapify(nums []int, i, size int) {
	// 堆数组化后左右子节点位置
	left, right, largest := 2*i+1, 2*i+2, i
	// 记录父结点和子节点的最大值
	if left < size && nums[left] > nums[largest] {
		largest = left
	}
	if right < size && nums[right] > nums[largest] {
		largest = right
	}
	// 如果记录的值不等于父结点，则交换并且重新调整当前结点的堆
	if i != largest {
		nums[i], nums[largest] = nums[largest], nums[i]
		maxHeapify(nums, largest, size)
	}
}
